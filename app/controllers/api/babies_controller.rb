class Api::BabiesController < ApiController
  before_action :set_baby, only: [:activity_logs]

  def index
    babies = []
    Baby.all.paginate(page:params[:page], per_page: 10).each do |baby|
      age = baby.age_to_months
      babies << {name: baby.name, baby_age: age, father_name: baby.father_name, mother_name: baby.mother_name, address: baby.address, phone: baby.phone}
    end
    render json: babies
  end

  def activity_logs
    activities= []
    if @baby.activity_logs.present?
      @baby.activity_logs.paginate(page:params[:page], per_page: 10).each do |activity_log|
        activity_log.stop_time.present? ? stop_date= activity_log.stop_time : stop_date = ''
        activities << {activity_log_id: activity_log.id, baby_id: @baby.id, assistant: activity_log.assistant.name, start_time: activity_log.start_time.iso8601, stop_time: stop_date}
      end
    else
      activities = {baby_id: @baby.id, alert: "sin actividades"}
    end
    render json: activities
  end

  private

    def set_baby
      @baby = Baby.find(params[:id]) rescue nil
    end
end