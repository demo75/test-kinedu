class Api::ActivityLogsController < ApiController
  before_action :set_activity_log, only: [:update]
  
  def create
    @activity_log = ActivityLog.new(params_activity_logs)
    if @activity_log.save
      render json: @activity_log, status: 200
    else
      render json: {error: @activity_log.errors.full_messages}, status: 400
    end                                                                                                   
  end

  def update
    if @activity_log.update(params_activity_logs)
      render json: @activity_log, status: 200
    else
      render json: {error: @activity_log.errors.full_messages}, status: 400
    end
  end

  private
    def params_activity_logs
      params.require(:activity_log).permit(:activity_id, :baby_id, :assistant_id, :start_time, :stop_time, :comments)
    end

    def set_activity_log
      @activity_log = ActivityLog.find(params[:id]) rescue nil
    end
end