class Api::ActivitiesController < ApiController
  def index
    @activities = Activity.select(:id, :name, :description).paginate(page:params[:page], per_page: 5)
    render json: @activities
  end
end