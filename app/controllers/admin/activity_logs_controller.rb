class Admin::ActivityLogsController < ApplicationController

  def index
    @activity_logs = ActivityLog.start_time_desc.paginate(page: params[:page], per_page: 10)
    filtering_params(params).each do |key, value|
      @activity_logs = @activity_logs.public_send("filter_by_#{key}", value) if value.present?
    end
    @babies = Baby.order_name
    @assistants = Assistant.order_name

  end

  def filtering_params(params)
    params.slice(:baby, :assistant, :status)
  end
end