class ActivityLog < ApplicationRecord
  
  belongs_to :activity
  belongs_to :assistant
  belongs_to :baby

  after_update :calc_duration

  validates :start_time_before_type_cast, format: {
    with:  /\A\d{4}-\d{2}-\d{2} \d{2}:\d{2}\z/
  }, presence: true
  validates :stop_time_before_type_cast, format: {
    with:  /\A\d{4}-\d{2}-\d{2} \d{2}:\d{2}\z/
  }, unless: :stop_time_nil?
  validate :validate_stop_time, unless: :stop_time_nil?

  scope :start_time_desc,     -> { order(start_time: :desc) }
  scope :filter_by_baby,      -> (baby) {joins(:baby).where(babies: {name: baby}) }
  scope :filter_by_assistant, -> (assitant) {joins(:assistant).where(assistants: {name: assitant})}
  scope :filter_by_status,    ->(status){
                                          if status== "finished"
                                            where.not(duration: nil)
                                          elsif status== "in_progress"
                                            where(duration: nil)
                                          elsif status == "all"
                                            all
                                          end
                                        } 

  def validate_stop_time
    if (stop_time <= start_time)
      errors.add(:stop_time, " no puede ser previo al la fecha de inicio")
    end
  end

  def stop_time_nil?
    stop_time.nil?
  end

  def calc_duration
    unless stop_time.nil?
      time_diff = (stop_time - start_time)
      dtion= (time_diff / 1.minute).round
      if update_column("duration", "#{dtion}")
        p "updateo"
      else
        p errors.full_messages
      end
    end
  end
end