class Baby < ApplicationRecord
  has_many :activity_logs, dependent: :destroy

  scope  :order_name,   -> {order(name: :asc).pluck(:name) }

  def age_to_months
    total_months = ((Date.today.year * 12 + Date.today.month) - ((birthday.year * 12) + birthday.month))
    age_years = total_months / 12
    age_months = total_months % 12
    a_years= ""
    age_years >= 1 ? a_years= "#{age_years} años" : a_years= ""
    age_months >= 1 ? m_months= "#{age_months} meses" : m_months= ""
    "#{a_years} #{m_months}"
  end
end