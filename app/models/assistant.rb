class Assistant < ApplicationRecord
  has_many :activity_logs, dependent: :destroy

  scope  :order_name,   -> { order(name: :asc).pluck(:name) }
end