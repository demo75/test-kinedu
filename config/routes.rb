Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  devise_scope :user do
    authenticated :user do
      root 'admin/activity_logs#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  namespace :admin do
    resources :activity_logs
  end

  namespace :api do
    devise_for :users, skip: %i[registrations sessions passwords]
    devise_scope :user do
      post '/signup', to: 'registrations#create'
      post '/login', to: 'sessions#create'
      delete '/logout', to: "sessions#destroy"
    end
    resources :activities, only:[:index]
    resources :babies, only: [:index] do
      collection do
        get ':id/activity_logs' => :activity_logs, as: :activity_logs
      end
    end
    resources :activity_logs, only: [:create, :update]
  end

end
